﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitpoints : Resource
{
    public Hitpoints(float total) : base(total)
    {
    }
    public override string ToString()
    {
        return current + "/" + total;
    }
}
