using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Endurance : Resource
{
    public float recovery;
    public Endurance(float total, float recovery) : base(total)
    {
        this.recovery = recovery;
    }
}
