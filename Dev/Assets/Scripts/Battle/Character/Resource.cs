using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Resource
{
    public float total;
    public float current;
    public Resource(float total)
    {
        this.total = total;
        this.current = total;
    }
}
