using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : Action
{
    public float damage;
    public Crit crit;
}