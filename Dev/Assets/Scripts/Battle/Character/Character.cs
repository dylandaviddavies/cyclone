﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
	public Hitpoints hitpoints;
	public Magicka magicka;
	public Endurance endurance;
	public Moveset moveset;
}
