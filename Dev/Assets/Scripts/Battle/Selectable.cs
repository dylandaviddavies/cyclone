
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    void OnMouseDown()
    {
        Character character = GetComponent<Character>();
        if (character is Hero)
        {
            GameManager.instance.battleMaster.ChooseCharacter(character);
            MenuPanel.Get().Open();
        }
        else if (character is Enemy)
        {
            if (GameManager.instance.battleMaster.chosenCharacter == null)
            {
                Toast.MakeToast("Woah! Slow down, partner! You need to select a hero first.", Resources.Load<Texture>("cowboyrob"));
            }
            else
            {
                GameManager.instance.battleMaster.ChooseTarget(character);
            }
        }
    }
}