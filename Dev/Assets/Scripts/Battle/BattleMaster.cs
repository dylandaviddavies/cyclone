using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleMaster
{
    public Character chosenCharacter;
    public Action chosenAction;
    private List<Character> exhaustedCharacters;
    public void ChooseCharacter(Character character)
    {
        if (exhaustedCharacters != null && exhaustedCharacters.Contains(character))
            return;
        chosenCharacter = character;
    }
    public void ChooseTarget(Character character)
    {
        if (chosenAction is Attack)
        {
            Attack(character);
        }
        FinishAction();
    }

    private void Attack(Character character)
    {
        Attack attack = (Attack)chosenAction;
        character.hitpoints.current -= attack.damage;
        GameObject canvas = UI.GetCanvas();
        FloatingText floatingText = FloatingText.Create();
        floatingText.SetText("" + attack.damage);;
        floatingText.transform.SetParent(canvas.transform, false);
        Vector2 screenPos = Camera.main.WorldToScreenPoint(character.transform.position);
        floatingText.transform.position = screenPos;
        if (character.hitpoints.current <= 0)
        {
            GameObject.Destroy(character.gameObject);
        }
    }

    public void FinishAction()
    {
        chosenCharacter = null;
        chosenAction = null;
        ExhaustCharacter(chosenCharacter);
    }
    public void ExhaustCharacter(Character character)
    {
        if (exhaustedCharacters == null)
            exhaustedCharacters = new List<Character>();
        exhaustedCharacters.Add(chosenCharacter);
    }
    public void ChooseAction(Action action)
    {
        chosenAction = action;
    }
    public void EndTurn()
    {
        exhaustedCharacters = null;
    }
}