﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public BattleMaster battleMaster;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        Init();
    }
    private void Init()
    {
        battleMaster = new BattleMaster();
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        {
            cube.AddComponent<Selectable>();
            Hero hero = cube.AddComponent<Hero>();
            {
                hero.hitpoints = new Hitpoints(100);
                hero.magicka = new Magicka(100);
                hero.endurance = new Endurance(100, 25);
                Moveset moveset = new Moveset();
                {
                    List<Attack> attacks = new List<Attack>();
                    {
                        Attack spinKick = new Attack();
                        {
                            spinKick.damage = 500f;
                            spinKick.crit = new Crit();
                            spinKick.name = "Spin Kick";
                        }
                        attacks.Add(spinKick);
                        Attack tackle = new Attack();
                        {
                            tackle.damage = 7f;
                            tackle.crit = new Crit();
                            tackle.name = "Tackle";
                        }
                        attacks.Add(tackle);
                    }
                    moveset.attacks = attacks;
                }
                hero.moveset = moveset;
            }
            cube.transform.position = new Vector3(0f, 0f, 0f);
        }
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        {
            sphere.AddComponent<Selectable>();
            Enemy enemy = sphere.AddComponent<Enemy>();
            {
                enemy.hitpoints = new Hitpoints(100);
                enemy.magicka = new Magicka(100);
                enemy.endurance = new Endurance(100, 25);
                Moveset moveset = new Moveset();
                {
                    List<Attack> attacks = new List<Attack>();
                    {
                        Attack spinKick = new Attack();
                        {
                            spinKick.damage = 5f;
                            spinKick.crit = new Crit();
                            spinKick.name = "Spin Kick";
                        }
                        attacks.Add(spinKick);
                        Attack tackle = new Attack();
                        {
                            tackle.damage = 7f;
                            tackle.crit = new Crit();
                            tackle.name = "Tackle";
                        }
                        attacks.Add(tackle);
                    }
                    moveset.attacks = attacks;
                }
                enemy.moveset = moveset;
            }
            sphere.transform.position = new Vector3(-5f, 0f, 0f);
        }
    }
}
