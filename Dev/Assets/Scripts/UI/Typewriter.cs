﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Typewriter : MonoBehaviour
{
    public Text textBox;
    public void Type(string text)
    {
        Type(text, null);
    }
    public void Type(string text, TypewriterCallback callback)
    {
        StartCoroutine(Typing(text, callback));
    }

    public void Empty()
    {
        textBox.text = "";
    }

    private IEnumerator Typing(string text, TypewriterCallback callback)
    {
        Empty();
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < text.Length + 1; i++)
        {
            this.textBox.text = text.Substring(0, i);
            yield return new WaitForSeconds(0.1f);
        }
        if (callback != null)
        {
			yield return new WaitForSeconds(callback.waitInSeconds);
            callback.action();
        }
    }
}
