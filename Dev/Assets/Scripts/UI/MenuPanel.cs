﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour
{
    public static MenuPanel Get()
    {
        return GameObject.Find("MenuPanel").GetComponent<MenuPanel>();
    }
    public void Open()
    {
        GetAnimator().SetBool("IsOpen", true);
    }
    public void Close()
    {
        GetAnimator().SetBool("IsOpen", false);
    }
    public Animator GetAnimator()
    {
        return GetComponent<Animator>();
    }
}
