using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    private static GameObject canvas;
    public static GameObject GetCanvas()
    {
        if (canvas == null)
            canvas = GameObject.Find("Canvas");
        return canvas;
    }
}