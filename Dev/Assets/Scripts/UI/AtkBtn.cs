﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtkBtn : MonoBehaviour
{
    public Action action;
    public ActionPanel panel;
    public void Click()
    {
        GameManager.instance.battleMaster.ChooseAction(action); 
        panel.Close();       
        panel.DestroyButtons();
    }
}
