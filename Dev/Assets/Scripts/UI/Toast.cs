﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Toast : MonoBehaviour
{
    public Animator animator;
    public Typewriter typewriter;
    public RawImage image;
    public static Toast Get()
    {
        return GameObject.Find("Toast").GetComponent<Toast>();
    }
    public static void MakeToast(string text, Texture texture)
    {
        Toast toast = Get();
        toast.UpdateImage(texture);
        toast.Open();
        toast.UpdateText(text);
    }
    public void UpdateImage(Texture texture)
    {
        image.texture = texture;
    }
    public void UpdateText(string message)
    {
        typewriter.Empty();
        typewriter.Type(message, new TypewriterCallback(Close, 1f));
    }
    public void Open()
    {
        animator.SetBool("IsOpen", true);
    }
    public void Close()
    {
        animator.SetBool("IsOpen", false);
    }
}
