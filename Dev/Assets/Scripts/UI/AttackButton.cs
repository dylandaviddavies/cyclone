﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackButton : MonoBehaviour
{
    public void Click()
    {
        MenuPanel.Get().Close();
        ActionPanel attackPanel = ActionPanel.Get();
        List<Attack> attacks = GameManager.instance.battleMaster.chosenCharacter.moveset.attacks;
        attackPanel.Init(attacks.ConvertAll(x => (Action)x));
        attackPanel.Open();
    }
}
