public class TypewriterCallback
{
    public System.Action action;
    public float waitInSeconds;
    public TypewriterCallback(System.Action action, float waitInSeconds)
    {
        this.action = action;
        this.waitInSeconds = waitInSeconds;
    }
}
