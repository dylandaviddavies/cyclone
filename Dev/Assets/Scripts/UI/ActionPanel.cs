﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionPanel : MonoBehaviour
{
    public static ActionPanel Get()
    {
        return GameObject.Find("ActionPanel").GetComponent<ActionPanel>();
    }

    public void Init(List<Action> actions)
    {
        DestroyButtons();
        float offset = -30;
        foreach (Action action in actions)
        {
            Vector3 thisPosition = transform.position;
            Vector3 position = new Vector3(thisPosition.x, thisPosition.y + 120 + offset, thisPosition.z);
            GameObject go = Instantiate(Resources.Load("AtkBtn"), position, Quaternion.identity) as GameObject;
            AtkBtn atkBtn = go.GetComponent<AtkBtn>();
            atkBtn.action = action;
            atkBtn.panel = this;
            atkBtn.GetComponentInChildren<Text>().text = action.name;
            atkBtn.transform.parent = transform;
            offset += offset;
        }
    }

    public void DestroyButtons()
    {
        foreach (GameObject o in GameObject.FindGameObjectsWithTag("AtkBtn"))
        {
            Destroy(o);
        }
    }

    public void Open()
    {
        GetAnimator().SetBool("IsOpen", true);
    }
    public void Close()
    {
        GetAnimator().SetBool("IsOpen", false);
    }
    public Animator GetAnimator()
    {
        return GetComponent<Animator>();
    }
}
